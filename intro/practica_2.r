## Practica 2
## Estructura de datos

list("lunes", "martes", "miercoles", "teoria", "practica") -> semana
print("-- Semana con otras cosas --")
print(semana)
print(semana[[3]])
print("-- fin  --")

mes <- list("febrero", "marzo", "abril")
meses_semana <- c(semana, mes)
print("-- Mes y otras cosas --")
print(meses_semana)
print(meses_semana[[5]])
print("-- fin --")

# concatenar las listas
c(semana, mes) -> mezclaRoot
print("-- concatenacion --")
print(mezclaRoot)
# pues hay 2 formas de hacer el listado con el signo de dolar...
# primero se tiene que nombrar cada elemento en la lista
mezclaA <- mezclaRoot
mezclaB <- mezclaRoot

# 1.- opcion...
names(mezclaA) <- c(1:8)
print(mezclaA$`5`) # imprimir el 5to elemento de la lista
# 2.- opcion...
names(mezclaB) <- mezclaB
print(mezclaB$practica) # ambas son validas
print("-- fin --")

hijos <- c("luis", "carlos", "eva")
edades <- c(7,5,3)
familia <- list(padre="juan", madre="maria", hijos.numero=3, hijos.nombre=hijos, hijos.edades=edades, ciudad="lugo")
print("-- Familia --")
print(familia)
print("nombres")
nombresHelper <- c(familia$hijos.nombre, familia$padre, familia$madre)
print(nombresHelper)
print("nombre del padre")
print(familia$padre)
print("numero de hijos")
print(familia$hijos.numero)
print("-- fin --")

# 3: Matrices y arrays
print("--- Matrices ---")
## matrix de 1 columna y enumeracion del 1 al 8
matrix(1:8) -> m_numbers
print(m_numbers)
## matrix de 4 columns (debido a que especificamos que solamente 2 files/rows) y enumeracion del 1 al 8
matrix(1:8, nrow=2) -> m_horizontal_numbers
print(m_horizontal_numbers)
## byrow (bool) hace que el llenado de la matriz sea por lineas/rows en lugar de por columnas (default)
matrix(1:8, nrow=2, byrow=TRUE) -> m_byrowT
print(m_byrowT)
## matriz de 2 columnas y 4 lineas/rows
matrix(1:8, ncol=2) -> m_vertical_numbers
print(m_vertical_numbers)

print("Length de m_numbers")
print(length(m_numbers))

print("Tipo de datos de m_numbers")
print(mode(m_numbers))

## Agregar una columna
print("Agregar una columna a m_numbers")
print(cbind(m_numbers, matrix(1:8)))

print("Agregar una entrada a m_numbers")
print(rbind(m_horizontal_numbers, matrix(1:4, nrow=1)))

matrix(c(22,55,160,40,70,180), nrow=2, byrow=TRUE) -> info
rownames(info) = c("Juan", "Pepe")
colnames(info) = c("Edad", "Peso", "Estatura")
print(info)
print("Info de Juan:")
print(info["Juan",])
## es los mismo que asignar cada una independiente...
dimnames(info) = list(c("Juan", "Pepe"), c("Edad", "Peso", "Estatura"))
print(info)
## aplicar la function "mean" en la matriz "info" (2 es aplicarlo por columnas)
# https://stat.ethz.ch/R-manual/R-devel/library/base/html/apply.html
print("Media de la info")
print(apply(info, 2, mean))

# https://www.tutorialspoint.com/r/r_arrays.htm
x1 <- array(1:20, dim=c(4,5))
print(x1)
print(x1[3,3])
# del f(x) = x & f(y) = y ... f(x) * f(y)
## multiplica cada valor de x por cada valor de y
## http://www.endmemo.com/program/R/outer.php
x2 <- outer(c(1:3), c(1:4), function(x, y) x*y)
print(x2)

# La differencia entre multiplicar matrices es que puedes aplicar una funcion entre vectores o arrays.
# al aplicar una funcion se vuelve mas flexible, aun mas, llenar una matriz a veces es mas dificil
# que tener dos vectores y aplicar una transformacion

a <- c(1:10)
my_data <- data.frame(a=a, b=a*a) # x**2
plot(my_data) ## if executed in terminal, will create a pdf file in the same folder

# https://www.stat.berkeley.edu/~s133/dates.html
fechas <- c("2002-06-09 12:45:40","2003-01-29 09:30:40","2002-09-04 16:45:40","2002-11-13 20:00:40", "2002-07-07 17:30:40")
eventos <- c('algo', 'a', 'v', 'b', 'fad')
agenda <- data.frame(fecha=fechas, evento=eventos)
print(agenda)

print("--- fin ---")
