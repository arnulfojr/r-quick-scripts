# Practica 3
agregar_cinco <- function(x) {
    x+5
}

print(agregar_cinco(4))

agregar_condicional <- function(x) {
    if (x > 5)
        x + 10
    else
        x + 5
}

print(agregar_condicional(4))
print(agregar_condicional(10))

asigancion_condicional <- function(a) {
    if (a > 0) {
        x <- 5
    } else {
        x <- 50
    }
    return(x)
}

print("Asignacion condicional")
print(asigancion_condicional(-1))

## 1.-
formula_una <- function(n) {
    return(n*(n+1)/2)
}
problema_uno <- function() {
   # es mejor un for loop porque creas una cadena del 1 al 20 e iteras calculando la formula
   # en el otro caso un while tiene que checar la condicion 
    for(i in 1:20) {
        print(formula_una(i))
    }
}

problema_dos <- function() {
    suma <- 0
    contador <- 0
    while(suma < 1000) {
        suma <- suma + contador
        contador <- contador + 1
    }

    print("Suma:")
    print(suma)

    print("Contador:")
    print(contador)
}

## Documentacion de las funciones en R
## http://rstudio-pubs-static.s3.amazonaws.com/93049_9bec20c7609a47faa3794a3f2d728c39.html

# https://es.wikipedia.org/wiki/Algoritmo_de_Euclides
# http://www.dma.fi.upm.es/recursos/aplicaciones/matematica_discreta/web/aritmetica_modular/divisibilidad.html
mcd_euclides <- function(a, b) {

    if (b == 0) {
        return(a)
    }

    if (a < 0 || b < 0 || a < b) {
        # este caso el digito no es valido
        return("no valido")
    }

    # en este caso a > 0 && b > 0 && a >= b
    while(b != 0) {
        resto <- a %% b
        a <- b
        b <- resto
    }
    return(a)
}

print("Problema 1:")
problema_uno()
print("Problema 2:")
problema_dos()
print("Euclides")
print(mcd_euclides(91, 0))
print(mcd_euclides(2366, 273))


