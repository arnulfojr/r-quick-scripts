# Practica 1
# vectores
# Martes 18 de abril

# crear un vector con valores [1, 6]
c(1,2,3,4,5,6) -> x1
print("Vector con valores [1, 6]")
print(x1)

# calcular 1/x con y sin asignacion...?
1/x1 -> x2
print("Resultado de 1/x")
print(x2)

# asignar un 7 a la variable y
7 -> y
print("La variable y con valor de 7")
print(y)

# asignar un vector a z
c(1,2,3,4,5,6,0,1,2,3,4,5,6) -> z
print("Z tiene el valor:")
print(z)
print("El valor minimo de z es:")
print(min(z))
print("El valor maximo de z es:")
print(max(z))
print("El 'length' de z es:")
print(length(z))
print("Una suma sencilla del maximo y minimo de z +1 es:")
print(min(z) + max(z) + 1)

# -- Sucesiones --
# del 1 al 30 incrementos de 1
c(1:30) -> s1  # no tiene sentido repetir la notacion
# c(1:30) es lo mismo que seq(1, 30)
print("Sequencia del 1 al 30")
print(s1)

seq(from=2, by=2, length=15) -> s2
print("Sequencia del 2 al 30, incrementos de 2")
print(s2)

print("sequencia del 1 al 30, incrementos en potencia")
# necesitamos [1, 5] 5*5 = 25 y 6^2 = 36 > 30
seq(from=1, by=1, length=5) -> helper
helper * helper -> s3
print(s3)

print("Empezar de 1, incrementar por 0.5 hasta que haya 5 elementos")
seq(1, 5, by=0.5) -> sx
print("Empezar de 1.5 incrementar 1 hasta que haya 5 elementos... hasta el 5.5")
seq(1.5, length=5) -> sy

# concatenar los vectores
c(s1, s2) -> sConcat
print("Concatenar s1 y s2")
print(sConcat)

# media y varianza de la primera sucesion
mean(s1) -> meanS1
var(s1) -> varS1

print("Media de s1")
print(meanS1)

print("Varianza de s1")
print(varS1)

# pregunta curiosa sobre rep(...)
# repite la list la cantidad de veces especificado
rep(helper, 1) -> rep1
rep(helper, 2) -> rep2
rep(helper, 3) -> rep3

print("Resultado de el comando rep(vector, times)")
print(rep1)
print(rep2)
print(rep3)

print("-- vectores logicos --")
# inizializar varX
c(1, 2, NA, 5) -> varX
print("original varX")
print(varX)
print("----------------------")
# is.na(vector) checa el contenido del vector, regresa TRUE si el elemento es NA
# FALSE si el caso contrario sucede.
print(is.na(varX))
# regresa el vector si es NA el *2 es "irrelevante"
# valores Boolean
is.na(varX*2) -> y
print(y)
# toma los valores del vector mayores que 2 y los asigna a la variable varX2
varX[varX > 2] -> varX2
# esto es algo curioso, pero la comparacion entre NA > 2 siempre devuelve NA
# por eso NA se encuantra en la variable varX2
print(varX2)
# toma los valores que no sean NA
varX[!is.na(varX)] -> varX3
print(varX3)



