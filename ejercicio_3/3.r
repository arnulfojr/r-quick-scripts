#!/usr/local/bin/r

## -- 3.- -- ##

# 1.-
# calcular los primeros 20 primeros terminos se la sucesion
# n * (n + 1) / 2
problema_1 <- function(limit) {

  sucesion <- function(val) {
    return(val * (val + 1) / 2)
  }

  # -- for loop -- #
  print('Problema 1: for loop (mejor caso)')
  for (i in 1:limit) {
    print(sucesion(i))
  }

  # -- while loop -- #
  i <- 1
  print('Problema 1: while loop')
  while (i <= limit) {
    print(sucesion(i))
    i <- i + 1
  }
  
  # -- repeat loop -- #
  i <- 1
  print('Problema 1: repeat loop')
  repeat {
    print(sucesion(i))
    i <- i + 1
    if (i > limit) break
  }
}

# 2.-
# sumar los primeros n numeros naturales hasta que la suma sea 1000
# n <- n + 1
problema_2 <- function(limit) {
  # -- while loop -- #
  print("While loop, mejor caso")
  # repite la suma mientras que la condicion se cumpla
  i <- 0
  suma <- 0
  while (suma < limit) {
    suma <- suma + i
    i <- i + 1
  }
  print(i)
  print(suma)

  # -- repeat loop -- #
  print("Repeat loop")
  # repite el codigo hasta que la condicion haga break
  i <- 0
  suma <- 0
  repeat {
    suma <- suma + i
    i <- i + 1
    if (suma >= limit) break
  }
  print(i)
  print(suma)

  # -- for loop -- #
  print("For loop")
  # no ha forma de saber con anticipacion para establecer el limite del rango
  # 1:limit debe ser siempre suficiente para sumar mas que limit
  # usar break en un for loop es considerado "bad practice"
  suma <- 0
  for (i in 1:limit) {
    suma <- suma + i
    if (suma >= limit) break
  }
  print(i)
  print(suma)
}

# -- correr el codigo -- #
problema_1(20)
problema_2(1000)
