#!/usr/local/bin/r

# 6.-
# euclides algorithm
# recursive
gcd <- function(a, b) {
  if (b == 0) {
    return(a)
  }

  # gcd(a, b) = gcd(b, r), donde r=residuo de a/b
  return(gcd(b, a %% b))
}

gcd(1071, 462)

# Loop basado en el residuo
gcd_2 <- function(a, b) {
  while (a != 0 && b != 0) {
    c <- b
    b <- a %% b
    a <- c
  }

  return(a + b)
}

gcd_2(1071, 462)


# loop v2 basado en el residuo
gcd_3 <- function(a, b) {
  while (b != 0) {
    r <- a %% b
    a <- b
    b <- r
  }

  return(a)
}

gcd_3(1071, 462)
